import View from './view';
import FighterView from './fighterView';
import FighterViewDetails from './fighterViewDetails';
import { fighterService } from './services/fightersService';
import detailsMap  from "./storage";

class FightersView extends View {

  constructor(fighters) {
    super();

    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);

  }

  createFighters(fighters) {
      const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event, fighter) {

    const modalWindow = document.getElementById("modalwindow");

    const modalContext = document.getElementById("detailsfighters");

    if(!detailsMap.gett().has(fighter._id)) {
      const fighterDetails = await fighterService.getFighterDetails(fighter._id);
      fighter = {...fighter, details: fighterDetails };
      detailsMap.sett(fighter._id, fighter);
    } else {
      fighter = await detailsMap.gett().get(fighter._id);
    }

    const fightersElement = new FighterViewDetails(fighter, event);
    modalContext.innerText='';
    modalContext.append(fightersElement.element);
    modalWindow.style.display = "block";

  }
}

export default FightersView;