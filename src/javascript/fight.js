import View from './view';
import FightView from './fightView';
import Fighter from './fighter';

class Fight extends View {

  constructor(fighters) {
    super();

    this.leftFighter = new Fighter(
        fighters[0].name,
        fighters[0].source,
        fighters[0].details.health,
        fighters[0].details.attack,
        fighters[0].details.defense
    );
    this.rightFighter = new Fighter(
        fighters[1].name,
        fighters[1].source,
        fighters[1].details.health,
        fighters[1].details.attack,
        fighters[1].details.defense
    );

    this.createFighters(this.leftFighter,this.rightFighter);

  }



  createFighters(leftFighter,rightFighter) {

    let leftF = new FightView(leftFighter, this.handleClick, 0, rightFighter);
    let rightF = new FightView(rightFighter, this.handleClick, 1, leftFighter);

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(leftF.element,rightF.element);
  }

  handleClick(index, fighter, rightFighter) {

    const divHeatlh = document.getElementById("health"+(index == 0 ? 1 : 0));
    rightFighter.setHealth(fighter.getHitPower());
    let value = Math.trunc(rightFighter.getHealth());
    console.log(value,rightFighter.getHitPower(),fighter, rightFighter)
    if (value <= 0) {
      const divWinModal = document.getElementById('modalwindowwin');
      divWinModal.style.display = 'block';
      const divTextWin = document.getElementById('messagewin');
      divTextWin.innerText = `${rightFighter.getName()} WON !!!`
    }
    divHeatlh.innerText = "Health: " + value;

  }

}
export default Fight;