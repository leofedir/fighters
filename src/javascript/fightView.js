import View from './view';

class FightView extends View {
  constructor(leftFighter, handleClick, index, rightFighter) {
    super();

    this.createFighter(leftFighter, handleClick, index, rightFighter);
  }

  createFighter(leftFighter, handleClick, index, rightFighter) {
    const nameElement = this.createName(leftFighter.getName());
    const imageElement = this.createImage(leftFighter.getSource());
    const healthElement = this.createHealth(leftFighter.getHealth(), index);
    const buttonElement = this.createButton(leftFighter, handleClick, index, rightFighter);

    this.element = this.createElement({ tagName: 'div', className: 'fighter' });
    this.element.append(imageElement, nameElement,healthElement,buttonElement);

  }

  createName(name) {
    const nameElement = this.createElement({ tagName: 'span', className: 'name' });
    nameElement.innerText = name;

    return nameElement;
  }

  createHealth(value, index) {
    const nameElement = this.createElement({ tagName: 'h6', className: 'name', id: "health"+index });
    nameElement.innerText = "Health: "+value;

    return nameElement;
  }

  createImage(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });

    return imgElement;
  }

  createButton(fighter, handleClick, index, rightFighter) {
    const nameElement = this.createElement({ tagName: 'button', className: 'name' });
    nameElement.innerText = "HIT !!!";
    nameElement.addEventListener('click',() =>
        handleClick(index, fighter, rightFighter));
    return nameElement;
  }

}

export default FightView;