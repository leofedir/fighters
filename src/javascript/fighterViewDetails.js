import View from './view';
import detailsMap from "./storage";
import fightArray from "./storageFight";
import Fight from './fight';

class FighterViewDetails extends View {
  constructor(fighter, handleClick) {
    super();

    this.showWindow(fighter, handleClick);

  }

  detailsFighter = {};
  currentFighter = {};

  showWindow(fighter) {
    const { name, details } = fighter;
    this.detailsFighter = details;
    this.currentFighter = fighter;
    const fieldsOfDetails = [
        { 'title':'Health:', value:details.health },
        { 'title':'Attack:', value:details.attack },
        { 'title':'Defense:', value:details.defense }
    ];

    this.element = this.createElement({ tagName: 'div', className: 'fighterdetails' });

    const span = this.createSpanElement();
    span.addEventListener('click',() => this.hideModalWindows(fighter,this.detailsFighter));
    let f =  [];
    f.push(this.createTitleNameFighter(`Name fighter: ${ name }`));

    fieldsOfDetails.forEach(item => {
      f.push(this.createTitle( item.title ));
      f.push(this.createInput( item.title, item.value ));
    });
    f.push(this.createHorisontalLine());
    f.push(this.createButton("FIGHT"));
    this.element.append( span,  ...f);

  }


  createSpanElement() {
    const nameElement = this.createElement({ tagName: 'span', className: 'close' });
    nameElement.innerText = '&times;';

    return nameElement;
  }

  createTitleNameFighter(text) {
    const nameElement = this.createElement({ tagName: 'h2', className: 'name' });
    nameElement.innerText = text;

    return nameElement;
  }

  createTitle(text) {
    const nameElement = this.createElement({ tagName: 'h6', className: 'name' });
    nameElement.innerText = text;

    return nameElement;
  }

  createInput(title, value) {
    const nameElement = this.createElement({ tagName: 'input', className: 'name' });
    nameElement.type = 'text';
    nameElement.value = value;
    nameElement.addEventListener('change',(e) => this.changeDetailsFighter(title,e));
    return nameElement;
  }

  createButton(text) {
    const nameElement = this.createElement({ tagName: 'button', className: 'name' });
    nameElement.innerText = text;
    nameElement.addEventListener('click',() =>
        this.fightButton(
          this.currentFighter,
          this.detailsFighter
    ));
    return nameElement;
  }

  createHorisontalLine() {
    const nameElement = document.createElement('hr');
    return nameElement;
  }

  hideModalWindows(fighter,details){
    const modalWindow = document.getElementById("modalwindow");
    modalWindow.style.display='none';
    fighter.details = details;
    detailsMap.sett(fighter._id, fighter);
  }

  fightButton(fighter,details){
    const f = document.getElementById(fighter._id);
    f.style.backgroundColor = 'coral';
    this.currentFighter.details = details;
    if (fightArray.gett().length<2) {
      fightArray.sett(this.currentFighter);
      if (fightArray.gett().length === 2) {
        this.hideModalWindows(fighter,details);
        this.activateFight(fightArray);
        return;
      }
    }
    this.hideModalWindows(fighter,details)
  }

  activateFight(fightArray){
    const rootWindow = document.getElementById("root");
    const figthWindow = document.getElementById("fight");
    rootWindow.style.display = 'none';
    const fight = new Fight(fightArray.gett());
    const fightElement = fight.element;
    figthWindow.appendChild(fightElement);
  }


  changeDetailsFighter(title, event){
    let value;
    try {
      value =  +event.target.value;
    } catch (e) {
      console.warn('Bad value ',e);
      return;
    }
    switch (title) {
      case 'Health:':
        this.detailsFighter.health = value;
        break;
      case 'Attack:':
        this.detailsFighter.attack = value;
        break;
      case 'Defense:':
        this.detailsFighter.defense = value;
        break;
    }

  }

}

export default FighterViewDetails;