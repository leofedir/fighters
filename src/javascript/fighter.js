class Fighter {
    constructor(name, source, health, attack, defense) {
        this.name = name;
        this.source = source;
        this.health = health;
        this.attack = attack;
        this.defense = defense;
    }

    getName(){
        return this.name;
    }

    getSource(){
        return this.source;
    }

    getHealth(){
        return this.health;
    }

    getHitPower(){
        return this.attack * Math.random();
    }

    getBlockPower(){
        return this.defense * Math.random();
    }

    getHealth(){
        return this.health;
    }


    setHealth(hitFighter) {
        let blockPower =  this.getBlockPower();
        blockPower = hitFighter < blockPower ? hitFighter : blockPower;
        this.health = this.health -
            (hitFighter -  blockPower);
    }


}

export default Fighter;