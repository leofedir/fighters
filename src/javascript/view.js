class View {
  element;

  createElement({ tagName, className = '', attributes = {}, id='' }) {
    const element = document.createElement(tagName);
    element.classList.add(className);
    if (id !== '') element.setAttribute('id', id);
    Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

    return element;
  }
}

export default View;
