let detailsMap = (function () {
    let fightersDetailsMapArr = new Map();
    return {
        gett: function () {
            return fightersDetailsMapArr;
        },
        sett: function (id,fighter) {
            fightersDetailsMapArr.set(id,fighter);
        }

    }
}());

export default detailsMap;
